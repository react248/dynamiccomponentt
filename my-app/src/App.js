import "./App.css";
import SquareOrCircle from "./components/SquareOrCircle";

function App() {
  return (
    <div>
      <SquareOrCircle isCircle={true} />
      <SquareOrCircle isCircle={false} />
    </div>
  );
}

export default App;
