function SquareOrCircle(props) {
  let dynamicStyles = {
    width: "200px",
    height: "200px",
    backgroundColor: "blue",
    borderRadius: "50%",
  };
  let dynamicStylesSquare = {
    width: "200px",
    height: "200px",
    backgroundColor: "blue",
  };
  return (
    <div style={props.isCircle ? dynamicStyles : dynamicStylesSquare}></div>
  );
}

export default SquareOrCircle;
